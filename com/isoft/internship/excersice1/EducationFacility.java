package com.isoft.internship.excersice1;
import java.util.ArrayList;
import java.util.List;

public abstract class EducationFacility {
	
	private String id;
	private String name;
	private String address;
	private double tax;
	private double averageGradeOfStudents;
	private List<Student> students;
	
	public EducationFacility(String id,
			String name, 
			String address, 
			double tax) {
		
		this.id = id;
		this.name = name;
		this.address = address;
		this.tax = tax;
		
		this.students = new ArrayList<>();
		
	}
	
	public void calculateAvrgGrade() {
		double avrgGrade = 0;
		for(Student s : this.students) {
			avrgGrade += s.getAvrgGrade();
		}
		
		avrgGrade = avrgGrade / this.students.size();

		this.averageGradeOfStudents = avrgGrade;
	}
	
	public double getAverageGrade() {
		return this.averageGradeOfStudents;
	}
	
	public void addStudent(Student student) {
		
		this.students.add(student);
		
		student.addToEducationFacility(this);
		
		System.out.printf("Hello %s and welcome to %s%n",
				student.getName(), this.getName());
	}
	
	public double getIncome() {
		double income = 0;
		
		for(Student student : this.students) {
			income += student.calculatePayment();
		}
		
		return income;
	}
	
	public List<Student> getStudents(){
		return this.students;
	}
	
	public double getTax() {
		return this.tax;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return " [name=" + name + ", address=" + address + "]";
	}


	
}
