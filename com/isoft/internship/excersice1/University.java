package com.isoft.internship.excersice1;

public class University extends EducationFacility{

	public static final double TAX = 100;
	
	public University(String id, String name, String address) {
		super(id, name, address, TAX);
	}

	@Override
	public String toString() {
		return "University " + super.toString();
	}

}
