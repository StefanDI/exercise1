package com.isoft.internship.excersice1;

public class Student {

	private String name;
	private Gender gender;
	private int age;
	private double avrgGrade;
	private String entityId;

	// For getting the educaionFacility TAX and average grade(for calculate payment
	// method)
	private EducationFacility studiesAt;

	public Student(String name, int age, double avrgGrade, Gender gender) {

		this.name = name;
		this.age = age;
		this.avrgGrade = avrgGrade;
		this.gender = gender;

	}

	public double calculatePayment() {
		return ((this.age / this.studiesAt.getAverageGrade()) * 100) + this.studiesAt.getTax();
	}

	public void addToEducationFacility(EducationFacility ef) {
		this.studiesAt = ef;
		this.entityId = ef.getId();
	}

	public String getName() {
		return name;
	}

	public Gender getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}

	public double getAvrgGrade() {
		return avrgGrade;
	}

	public String getEntityId() {
		return entityId;
	}

	@Override
	public String toString() {

		return String.format("Name: %s, Gender: %s, Age:%s, AvrgGrade: %.2f%n",
				name,
				gender.toString().toLowerCase(),
				age,
				avrgGrade);
	}

}
