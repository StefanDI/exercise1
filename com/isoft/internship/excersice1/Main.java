package com.isoft.internship.excersice1;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class Main {

	private static final String[] MALE_NAMES =
		{"Ivan", "Georgi", "Stefan", "Petyr", "Misho", "Dean"} ;
	private static final String[] FEMALE_NAMES = {"Ivana", "Nadejda", "Yoana", "Valia"};
	
	private static List<EducationFacility> educationFacilities = new ArrayList<>(5);
	
	public static void main(String[] args) {
		
		generateEducationBuildings();
			
		String topContributorName = "";
		double currentTopContribution = Double.MIN_VALUE;

		//SolarLint suggested using EnumMap
		Map<Gender, Student> topPerformerByGender = new EnumMap<>(Gender.class);
		
		//Put dummy students with lowest grade
		topPerformerByGender.put(Gender.MALE, new Student("dummy", 0, 2, Gender.MALE));
		topPerformerByGender.put(Gender.FEMALE, new Student("dummy", 0, 2, Gender.FEMALE));
		
		double totalIncome = 0;
		double maxIncome = 0;
		
		for (int i = 0; i < 50; i++) {
			
			Student student = generateRandomStudent();		
		
			//first 0 - 9 will be in first facility, 10 - 19 in second etc...
			educationFacilities.get(i / 10).addStudent(student);
			
			
			//Check if the current student is top performer of his gender
			Student currentTopPerformer = topPerformerByGender.get(student.getGender());
			if(student.getAvrgGrade() > currentTopPerformer.getAvrgGrade())
				topPerformerByGender.put(student.getGender(), student);
			
			
			
			//9 , 19, 29,....
			//We added all the students to this entity,  make calculations
			if(i > 0 && i % 10 == 9) {
				

				EducationFacility currentFacility = educationFacilities.get(i / 10);
				currentFacility.calculateAvrgGrade();
				
				double currentIncome = currentFacility.getIncome();
				
			
				totalIncome += currentIncome;
				
				if(currentIncome > maxIncome) {
					maxIncome = currentIncome;
				}
				
				
				System.out.printf("Average grade of students in %s is %.2f%n%n",
						currentFacility.getName(), currentFacility.getAverageGrade());
				
				
				for(Student s : currentFacility.getStudents()) {
					if(student.calculatePayment() > currentTopContribution)
						topContributorName = student.getName();
				}
			}
			
			
		}		

		
		System.out.printf("Total income from all facilities is %.2f%n%n",
				totalIncome);
	
	
		if(topPerformerByGender.get(Gender.MALE).getAvrgGrade() > topPerformerByGender.get(Gender.FEMALE).getAvrgGrade())
			System.out.println("Top performing student from all facilities is:\n " + topPerformerByGender.get(Gender.MALE));
		else
			System.out.println("Top performing student from all facilities is:\n " + topPerformerByGender.get(Gender.FEMALE));

		System.out.printf("Top performing male is: %sTop performing female is:%s%n",
				topPerformerByGender.get(Gender.MALE),
				topPerformerByGender.get(Gender.FEMALE));
		
		System.out.println("Top contributor's name is: " + topContributorName);
		
		
	}
	
	private static Student generateRandomStudent() {
		Random r = new Random();
		
		//Random grade between 2 and 6
		double randomGrade = round(2 + (6 - 2) * r.nextDouble(), 2);
		
		boolean isMale = r.nextBoolean();
		
		Gender randomGender = isMale ? Gender.MALE : Gender.FEMALE;
		
		//If is male, randomIndex is from 0 to MALE_NAMES length else from 0 to FEMALE_NAMES.length
		int randomNameIndex =
				r.nextInt(isMale ? MALE_NAMES.length : FEMALE_NAMES.length);
		
		String randomName = isMale ? MALE_NAMES[randomNameIndex] : FEMALE_NAMES[randomNameIndex];
		
		// random age between 7 - 37
		int randomAge = r.nextInt(30) + 7; 
		
		return new Student(randomName,randomAge,randomGrade,randomGender);
	}
	private static void generateEducationBuildings() {
		educationFacilities.add(new School(UUID.randomUUID().toString() , "Ivan Vazov", "Ul Batovska 3"));
		educationFacilities.add(new School(UUID.randomUUID().toString() , "Geo Milev", "Ul Izgrev 5"));
		educationFacilities.add(new School(UUID.randomUUID().toString() , "Petyr Beron", "Ul Pavlova 13"));
		
		educationFacilities.add(new University(UUID.randomUUID().toString(), "Nikola Vapcarov", "Ul Opalchenska 11"));
		educationFacilities.add(new University(UUID.randomUUID().toString(), "Georgi Rakovski", "Ul Vitoshka 21"));
		
	}
	
	//Util method for rounding
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
