package com.isoft.internship.excersice1;

public class School extends EducationFacility{
	public static final double TAX = 50;

	public School(String id, String name, String address) {
		super(id, name, address, TAX);
	}

	@Override
	public String toString() {
		return "School " + super.toString();
	}

}
